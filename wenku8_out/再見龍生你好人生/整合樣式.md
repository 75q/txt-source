---
LocalesID: 再見龍生你好人生
LocalesURL: https://github.com/bluelovers/node-novel/blob/master/lib/locales/%E5%86%8D%E8%A6%8B%E9%BE%8D%E7%94%9F%E4%BD%A0%E5%A5%BD%E4%BA%BA%E7%94%9F.ts
---
__TOC__

[再見龍生你好人生](https://github.com/bluelovers/node-novel/blob/master/lib/locales/%E5%86%8D%E8%A6%8B%E9%BE%8D%E7%94%9F%E4%BD%A0%E5%A5%BD%E4%BA%BA%E7%94%9F.ts)  
總數：50／51

# Pattern

## 尼祿妮西婭

### patterns

- `尼祿妮西亞`

## ・菲怜・艾奇爾尼亞

### patterns

- `#_@_#菲怜#_@_#艾奇爾尼亞`

## ・庫麗絲特・狄熙笛亞

### patterns

- `#_@_#庫麗絲特#_@_#狄熙笛亞`

## ・索蘭・艾奇爾尼亞

### patterns

- `#_@_#索蘭#_@_#艾奇爾尼亞`

## 瑪莉伊達

### patterns

- `マリーダ`
- `瑪莉達`

## 佩庫伽洛亞

### patterns

- `べくガロア`

## 迪婭多菈

### patterns

- `ディアドラ`

## 多菈米娜

### patterns

- `多菈米娜`
- `德拉米娜`
- `ドラミナ`

## 多菈醬

### patterns

- `德拉醬`

## 古羅斯格利亞

### patterns

- `古羅斯格利亞`
- `グロースグリア`
- `斯格里亞`

## 基歐爾

### patterns

- `ジオール`

## 科爾涅普

### patterns

- `ゴルネプ`

## 莉莉亞娜

### patterns

- `リリアナ`

## 杰歐

### patterns

- `ジャオ`

## 涅魯多納

### patterns

- `ネルトナ`

## 歐庫多烏魯

### patterns

- `オクトウル`

## 皮可

### patterns

- `ピコ`

## 阿魯阿娜

### patterns

- `アルアーナ`

## 帕拉霍姆

### patterns

- `パラホム`

## 庫拉伊拉

### patterns

- `クライラ`

## 諾布倫

### patterns

- `ノーブレン`

## 蕾婭

### patterns

- `レア`

## 卡拉維斯

### patterns

- `カラヴィス`

## 卡洛亞

### patterns

- `ガロア`

## 庫魯多耶

### patterns

- `クールドイエ`

## 拉歐謝恩

### patterns

- `ラオシェン`

## 庫夏烏拉

### patterns

- `クシャウラ`

## 萊多

### patterns

- `ライト`

## 蕾杰娜

### patterns

- `レジナ`

## 帕里歐

### patterns

- `パリオン`

## 藍色史萊姆

### patterns

- `ブルースライム`

## 龍人

### patterns

- `ドラゴ二アン`

## 冰霜標槍

### patterns

- `アイスジャベリン`

## 深紅之蓮

### patterns

- `クリムゾン・レイ`
- `Crimson Lei`

## 蓋亞衝撞

### patterns

- `ガイアストライク`
- `Gaia Strike`

## 冰粒耀斑

### patterns

- `アイスクルフレア`
- `Ice Particle Flare`

## 熾炎之劍

### patterns

- `バーニングエッジ`
- `Burning Edge`

## 冰霜之箭

### patterns

- `Ice bolt`

## 神聖標槍

### patterns

- `セレスティアルジャベリン`
- `天體標槍`
- `Celestial Javelin`

## 爆裂

### patterns

- `エクスプロージヨン`
- `Explosion`

## 地・超重壓

### patterns

- `ジオ・グラビオン`
- `Geo・Gravion`

## 邪毒噬

### patterns

- `ジャドゥ—ク`

## 邪拉姆

### patterns

- `ジャラ－ム`


