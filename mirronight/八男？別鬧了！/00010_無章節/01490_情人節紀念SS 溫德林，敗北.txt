﻿情人節紀念SS 溫德林，敗北

1-1.

帝國內亂結束後，我們回到平常的生活。

以冒險者的身份狩獵著，和恩斯特一起去探索地下遺蹟，進行鮑麥斯特伯爵領領地內的土木工程，偶爾還會陸續處理身為貴族該做的工作。

因為羅德裡西在行程安排得很好，所以並沒有發生什麼混亂。

季節來到冬季，這裡位處於大陸南部所以氣候很溫暖，但在謝肉祭和新年一過，月份轉變之時某位客人來訪了。

帶領他到鮑麥斯特伯爵官邸伯爵邸的客廳後，那位人物以輕柔的語調在詢問了。

「我一直在想，有沒有什麼東西可以拿來作為買賣的題材呢」

那位人物，就是鮑麥斯特伯爵家首席御用商人的阿爾提裡歐先生。

然而貴族家的御用商人，有所謂一人獨賣和複數商家之別。

我們家是複數派，不過，身為首席交易量也最多的阿爾提裡歐先生，其名字在王都變得很有名了。

其他還有別的御用商人，某種程度是一種防止嫉妒的作法。

導師家的次男亨利克(ヘンリック)也是(御用商人之一)，雖然順位比較低但還是被任命為鮑麥斯特伯爵家的御用商人了。 (註:ヘンリック，在布洛瓦篇中登場因為討厭導師的嚴厲管教而負氣去當商人，主要是做小型飛船的運輸)

他在內亂中將使用的小型魔導飛行船增加到了三艘，無拘無束地在鮑麥斯特伯爵領境內運輸著人和物品在賺錢。

「總覺得會不會有不安的因素在裡面？」

「雖然不會有不安因素，但巧克力事業要稍微轉換一下方針了」

在魔之森林裡採集到作為材料可可豆所製成的巧克力雖然在王都中販售了，不過，由於鮑麥斯特爵領的開發持續推進中使得冒險者所集中收集而來的可可豆數量大幅增加，好像造成巧克力的價格下跌。

「被採成那種程度沒問題嗎？」

「因為是從樹上將果實取下來，所以經過三天後就會恢復原狀了」

「真是可怕的繁殖力呢」

「比雜草還厲害啊」

關於可可豆，在地球上因需求量大增導致價格大幅上漲就結果來看結果都是一樣的。

對於可以自然地任意採摘我覺得很厲害。

由於進行採摘而被魔物殺死的人也有存在一定的人數，所以也有入手很困難這個理由在內。

「對我們來說價格下跌不是問題」

最起先巧克力的製造技術得到了確立，而成為高級品後得到了王族、大貴族們的御用。

還有，阿爾提裡歐先生說，以富裕的平民階層為銷售對象，生產出來的量產品已到了準備開始銷售的時期。

「價格也大幅下降了，如果是次級品即使是平民也能做到一個月可以吃上一次的程度了。只是，並非王都裡的所有住民全都知道。還是有很多人覺得這東西高不可攀」

「總之，為了普及開來必須得想辦法宣傳嗎？」

「不愧是鮑麥斯特伯爵大人啊。對買賣真是瞭解」

我覺得雖然我是貴族就算做生意的才能被稱讚了，但這也是因為以前曾做過這樣的工作而有的自豪。

不可能不高興。

作為社畜的每一天，絕對不是毫無用處的。

「會撒傳單做宣傳嗎？」

「那個也會做的喔。其實，我在想辦法編出一個讓人願意去購買巧克力的理由，舉辦活動什麼的」

「哎？」

我對阿爾提裡歐先生的發言，有種不好的預感了。

確實，現在月份才剛移往新的一年而已。

「（這種活動，是針對情人節而來的吧？）」

但是，不知如何身為這個世界之人的阿爾提裡歐先生會知道情人節。

事實上，雖然我是隱埋起來，但他會不會也是來自地球的轉生者呢？

不，如果是的話在聽我的意見之前，他自己應該早就會有各種各樣的做法了。

「怎麼了嗎？ 鮑麥斯特伯爵大人」

「稍微再想一些事情……」

或許，是我想太久。

讓阿爾提裡歐先生擔心起來了。

「和其他製造巧克力的商會一起合作，盛大舉辦『聖瓦倫丁祭』吧」

「誒？ 他是誰？」

確實，我有意思要將日本式的聖誕節普及開來，而瓦倫丁是暗黒宗教節日的『謝肉祭』中原本所存在的人物。

「是很有名的聖人喔。他的太太也是，都是留有相當出色藝文的人喔」

「喔，是這樣啊」

「鮑麥斯特伯爵大人，是教會的名譽司祭吧？

怎麼會不知道呢？」

「不，我真的不知道」

只是因捐款而接受正式的洗禮還被隨意地命名了，然而有關教會的事情我根本就不感興趣。

教會悠久的歷史成正比的就是聖人了，他們在過往中有很多是留下了卓越功績的偉人，以及名聲很好的人。

因為人數很多，如果不是非常有興趣的話要記住所有人是很辛苦的。

就算考試，若是沒有取得一定的分數而會留級的話，我想努力一下應該就記得起來了。

我認為，有限的記憶力不需要去記這些東西。

對人來說最重要的就是現在和未來。

那句話，曾在高中時期教日本史的老師有說過。

「平時，該稍微學習一點吧……。嗯，夫人？」

阿爾提裡歐先生對於我的無知感到很吃驚，但埃莉絲加進對話裡了。

「我想為此我才會在這裡。瓦倫丁樞機卿的夫人，也會親自將手工製作的點心發送給貧苦的孩子們，而且還會去瓦倫丁樞機卿所舉辦的活動幫忙」

如果這二個人有那個意思的話明明可以過著非常富足的生活，卻經常過得很清貧，為了奉獻活動即使連互贈禮物都沒做的樣子。

1-2.

「那二位，彼此都相互瞭解。就算沒有贈送禮物，還是互相愛著對方」

但是，姑且只有一次例外有贈過禮。

「他的夫人在先因病過世的瓦倫丁樞機輕的棺木內，悄悄地添上了親手製作的餅乾。那是作為夫婦，頭一次的贈禮」

埃莉絲以出神的表情在談論著過去聖人的逸聞。

確實，連我都覺得『真是一段佳話啊』。

特別是，讓女性難以控制情緒的故事。

此時並不認為是『為孩子所做而剩下來的餅乾，把這個送給丈夫就行了』。

將視線朝週遭看過去時，遙、伊娜、露易絲、維爾瑪、卡特莉娜都似乎對那件逸聞很感動。

「真是浪漫的愛情故事」

「親手製作的餅乾這段說的真好」

「在二人之間所產生的真愛呢」

「好感動」

「雖然我知道這個故事，但不管怎麼聽都覺得很浪漫」

如果我死了，說不定棺木裡會有滿滿的餅乾。

要是那樣，大家都會很感動吧。

「那麼，這段逸聞又跟巧克力有什麼關係呢？」

只有艾爾一個人很冷靜，對阿爾提裡歐先生提出了問題。

果然，女性和男性或許在思考方式上有所不同。

艾爾，看不出來有因那段佳話而露出感動的表情。

「因為啊，那是在瓦倫丁樞機卿過世那天，把巧克力啊……」

「跟巧克力沒關係吧？」

「如果硬要這麼說，講再多都沒問不是嗎」

為了做生意，有時也是需要某種遷強的手段這連我都知道。

阿爾提裡歐先生的提案，想讓平民可以到商家去購買巧克力點心或蛋糕然後大家再一起享用。

對富裕階層或貴族，則是在想點子讓女性去贈送男性巧克力。

「丈夫、未婚夫、父親、兄弟、甚至是在房子裡進行服侍的傭人都可以。如果是貴族，或許就能將單價下跌的巧克力大量地買下來了」

果然，那就是情人情了。

總覺得，贈送巧克力給對方感覺比較偏向於人情，不過，連在近年來的日本都有在宣傳可以將情人節的巧克力贈送給不同的人。

贈送給曾受過對方關照的人『關照巧克力』，贈給家人的『親人巧克力』，或是贈送給朋友的『友情巧克力』。

雖然很明顯是糖果製造商為了增加銷售額的陰謀，不過，原本情人節本身就是糖果製造商所想出來的陰謀了。(註:這讓譯者想起前幾年的台灣一到接近2/14的前幾週，最多的置入新聞就是餐廳旅館業。所以情人節常被戲稱是打砲節...。同理端午節成了肉粽節、中秋變成烤肉節)

而且，總覺得說到這個就覺得很可怕……。

「（贈送巧克力之前，就怕經常在換戀人了。不，即使照阿爾提裡歐先生說的那樣光是要有丈夫或未婚夫，要成為那種關係是很花時間的……）」

如果是那樣的話，相當多的男性會迎來黑暗情人節的。

沒錯，縱使我在前世，在情人節的時候『人情(巧克力)，百分之九十九是母親給的』好像都是這種情況。 (註:義理、母ちゃん九十九パーセント=男人的年齡等於沒有女友的時間=30歲的男性魔法使)

「（說起來，就只有人情巧克力……）」

很明顯得還三倍……現在還有五倍或十倍的都有。以白色情人節為目標要收到成堆的人情巧克力，唯一可以容忍的就是從母親那所得到的巧克力吧。

本命只有一次是在大學時代從女友的手裡得到的。

就業後，又從公司內的女性職員手上得到的人情巧克力。

在公司裡沒得到的人，大概都被認為是被人相當討厭，或是何時會消失都不奇怪的傢伙。

如果沒有得到可以說這個人在公司內沒有地位，但就算得到了還基本上是要回贈三倍數量的人情巧克力。

因為一個月後的白色情人節會讓我的錢包變薄，所以情人節對我來說只是如臨大敵而已。

就連工作，也會因巧克力的進口業務而變得忙碌起來。

大家都對進口巧克力抱持著幻想。

其中會有品質很差的商品，經常會發生要求賠償損失，因此我們在回收商品時就得到處去低頭道歉。

會不會是歐美的商業規則呢？

如果提出抗議，不知為何會有不講理的歐美方的負責人明明是加害人卻反過來對受害人發脾氣的。

因為還會有已經到了下班時間，話才講到一半就掛電話回家的人。

並非是我們的錯，雖然運送的方法太差或是不在意責任轉嫁的中國人很少，但不負責任的性格說不定和人種是無關的。

總之，我對情人節並沒有正經的回憶。

「（但是，為什麼阿爾提裡歐先生會這麼即要去推和情人節很相似的企劃？）」

或許，是受到我轉生所帶來的影響而產生出的蝴蝶效應嗎？

哎呀，會不會太過於臆測了呢？

關於贈送巧克力的活動，只要是稍微機靈的生意人總會想到的。

「鮑麥斯特伯爵大人，如何？」

「嗯……，嚐試看看如何？」

「嗯？ 總覺得沒什麼點子呢？

但是，一直以來都只是仰賴於鮑麥斯特伯爵大人的意見啊。明知不行還是要試著做看看」

就這樣，阿爾提裡歐先生決定要在王都舉辦山寨情人節。

雖然我沒有反對，但是內心裡卻不是這麼回事。

「（巧克力高興什麼時候吃就吃那就可以了！

我，絕對會破壞這個情人節的！）」

即使把任何事都擱置下來，只有(搞爛情節人)這件事我在心裡發誓絕對要去進行。

2.

「新活動？ 那算是貴族的工作嗎？

威爾」

「不是，但有部分相關呢……」

我立刻投入進行破壞情人節的行動了。

話雖如此，我覺得應該要正面地將埃莉絲她們覺得很感人的故事當題材所舉辦的活動給破壞才行。

不認為(情人節)會大受歡迎，但阿爾提裡歐先生畢竟是我們家的御用商人。

因此，讓他去進行其他流行的活動，而想實行『聖瓦倫丁祭不落實作戰』了。

雖然把艾爾叫來幫忙了，但我對他謊稱是在做『對聖瓦倫丁祭能否實施感到不安，所以正在規畫其他活動的提案』而讓他來幫忙。

「因為是我們這兒的特產品，所以讓巧克力普及開來不是很好嗎？」

「就是巧克力很好吃才要大力推廣。比起那件事……」

言歸正傳，若是要破壞情人節，就必須要有一個能超越它的魅力的活動。

在日本情人節是二月及同月份所舉行的活動，雖然舉辦那活動有如理所當然一樣，所以才會讓我下定決心要將那個想辦法普及開來的情人節給破壞掉。

雖然稍微缺乏闊氣感，不過，我認為要是能盛大起來是能與情人節相抗衡的。

反正，關於原本的活動知道的人是不存在的。

「那個提示，在瑞穗公爵領！」

於是，我和艾爾以『瞬間移動』飛往瑞穗公爵領。

這次雖然只有二個人移動過去，但那是因為不能讓站在舉辦聖瓦倫丁祭的遙同行造成的。

瑞穗公爵領正處於寒冬中所以很冷，不過，就跟預料中的一樣那個食物有在商店裡販售著。

「炒豆子？」

「這麼一來，任誰都會買的喔」

「因為很便宜吧……」

因為在王都，直到豆腐和腐竹普及開來為止都是被當成家畜的飼料可以說是用途最廣泛的食材。

正因『節分』有使用這種炒豆，所以應該是有辦法對抗情人節的。(註:節分，中文是立春的前一天。而所謂的節分祭就是在立春的前一天及當天的傍晚，將柊樹的樹枝插在門口，然後把炒好的大豆當成打鬼豆來撒的習俗)

不，我要把這個活動培植起來。

「鮑麥斯特伯爵大人，對『移節祭』感興趣啊？」

到達瑞穗公爵領後，聽到羅德裡西透過瑞穗公爵派剛臣先生來當我的護衛了。

今天的他還因為遙不在使得情緒變得不佳了。

「要讓這個移節祭，在王國普及開來」

「王國嗎？ 但是，移節祭。很乏味喔」

由來和節分很相似的移節祭，是在農曆季節交替之時將炒好的豆子拿來撒用來驅邪，如果撒出去的豆子和吃下與年齡相符的豆子數量，被認為那一整年將不會生病。

這可是和節分相當類似的活動。

「貴公司……為基礎，因為教會會朝當年吉利的方位將豆子撒出去，所以大家都會去撿呢……」

家人會有其中一人去撿拾所有人分的豆子，然後在當天夜裡分食。

還有，剛臣先生說說中提到會用柊和魚頭做成擺飾裝飾在家門口。

「啥？很無趣吧？」

「確實很無趣……」

「喂，威爾。炒好的豆子贏的過巧克力嗎？」

被艾爾刺耳地指謫了。

確實，好吃的巧克力，和平淡無奇的炒豆誰輸誰贏比都不用比。

果然，原本就很乏味的節分肯定是會被情人節給擊潰的。

即使在日本，那種傾向也很強烈。

但即使如此我依然要奮戰下去。

為了將這個世界未來的男性同胞導向幸福。

我的這場戰鬥，絕對不會有人能夠理解的。

即使如此，我還是要戰鬥。

這場孤獨的戰鬥！

為的是要將與情人節相似的活動給破壞掉！

「說不好吃倒也不會，也不至於會好吃吧」

艾爾，在店門口一邊吃著買來的炒豆一邊陳述著感想。

姑且是買來嚐腸味道的樣子。

「我覺得黃豆粉還比這好吃」

連剛臣先生也是，不認為炒豆贏的了巧克力。

「在瑞穗公爵領，因與鮑麥斯特伯爵大人有交情所以交易很繁盛喔。如果說巧克力的話，就在那邊……」

原本，是在賣和和式點心很相似的瑞穗點心的店舖都將特別櫃位給空出來，在那裡擺上了巧克力。

雖然價格很貴又很稀少，但還是有很多客人顯得很熱鬧。

「雖然我不喜歡那麼甜的甜食，但新產品的『抹茶巧克力』還蠻好吃的呢」

「（什麼！）」

衝撃般的事實。

雖說和日本人很相似，但瑞穗人已經將輸入進來的巧克力進行改良了。

「真的耶……」

店門口的特別櫃位上所擺放出來的巧克力點心，其中存在著抹茶口味。

試著買起來吃了，不過，就像是在日本所吃過的抹茶巧克力的一樣。

真的非常好吃……。

「其他還有……黃豆粉巧克力啊！」

瑞穗人真是欺負人！ 

居然連黃豆粉巧克力都有，其他也有巧克力饅頭、巧克力大福在販售著。

「改良能力真是完美啊」

「瑞穗人，很喜歡將從外面導入進來的料理和食品進行改良喔」


連那塊領域，都和日本人沒兩樣。

「如果連這些都出口出去的話……」

物以稀為貴，即使是貴族也會花大把銀子買下來的。

如果阿爾提裡歐先生注意到了，而拿來在聖瓦倫丁祭販賣的話……。

「再這麼下去不會有勝算的！」

我的危機感越來越強了。

「因為，作為巧克力原料的可可豆是我們家的特產，所以才會賣得很好不是嗎」

雖然艾爾說的沒錯，但我還是反對巧克力的普及。

為了這個世界的希望，而把山寨情人節的普及阻止下來就是我的目的。

「既然都變成這樣了，不只炒豆連其他食物都導入進來吧！」

「我，真是搞不懂威爾的想法」

「就算不能，當官就是如此」

剛臣先生，在對艾爾告誡了。

「如此，那我就配合吧……」

「那就好，艾爾溫喲」

不知為何，艾爾和剛臣先生的意見似乎一致了，如此一來就更好了我們三個人便開始進行活動的準備。

3.

「呼，我是消滅竜的鮑麥斯特伯爵。我所推薦的活動，不認為會輸給聖瓦爾丁祭的」

「就不知道威爾是從哪裡湧出那份自信的，不過，我不會感到奇怪了」

從那之後又過了幾天，我為了不讓情人節在這個世界普及而做了準備。

羅德裡西指示的土木工程完成的同時，還在進行要與炒豆一起普及開來的食物準備。

「那麼，完成的就是這個『恵方卷』！」(註:恵方巻，是大阪地區的一種特色壽司，裡面包著醃葫蘆條、黃瓜、雞蛋捲、鰻魚、肉鬆、椎茸等七種食材，代表七福神。在節分之日食用象徵將福氣捲起來吃，有去災避邪、祝願生意興榮的意思。此外，還有朝福卷、開運卷等稱呼)

「這個，是太卷吧？」

雖然有請瑞穗公爵稍微將剛臣先生稍微借我一下，不過，他在看到大量被準備好的惠方卷時向我提問了。

即使是在瑞穗公爵領，在移節祭中並不存在吃惠方卷的習俗。

就算是在日本，原本也只是在狹小的地域內才有的習俗。

也可以說是海苔店所佈置出來的活動，即使這個世界沒有也沒什麼好奇怪的。

「雖然是太卷，不過，內容物當然會做改良」

為了儘量配合王國人的口味，有煎蛋、肉鬆、黃瓜等東西其他還放入煮蝦、煙燻鮭魚、油漬鮪魚、炸物等東西，儘量改良到符合多數人的喜好。

「用這個，就能贏巧克力了！」

「能嗎？ 話說回來，這個惠方卷要怎麼吃比較好？」

「艾爾啊，只是吃是不行的。這樣子，是找不到幸運來訪的方向的……」

就這麼一口咬住，一個人吃一條。

隨著做這種事情……咦？

到底會怎樣？

這麼說來，雖然知道有惠方卷，但我不知道該怎麼吃。

「……祈求健康」

「什麼，突然間就想到一個很好的回答了」

艾爾開始用懷疑的目光看著我了。

「總而言之！ 把這個習俗擴大開來，並且販賣惠方卷！

連炒豆也順便！」

瓦倫丁樞機卿的忌日，在王都裡有二個活動要被推行。

一個是阿爾提裡歐先生和販賣巧克力的商會，要在店舖裡所舉辦的聖瓦倫丁祭。

另一個就是，我們所努力推廣的移節祭。

我們打算讓撒豆、吃惠方卷變成習俗。

「勝利肯定是我們！

艾爾！ 剛臣先生！」

「是是。難道，我們要直接變成銷售員……」

「當官的人的宿命啊……」

就這樣，雙方的宣傳戰開始了……。

當大家在討論為什麼主角很討厭情人節，其實給各提示"現充爆炸吧"

這句在宅宅圈中很流行，也就代表主角為什麼要搞破壞。

另外一個理由，就是在日本時期主角曾因為白色情人節三倍回禮而大傷荷包，對此仇恨值處在100中，連寧神射擊都解除不掉

"LING"4-1.

「賣得還算普普通通啊」 (註:売れている，這裡有二個解釋，一是"暢銷"、二是"賣出去"。因為作者沒解釋惠方卷賣出去多少以及有多少男性購買，針對惠方卷的暢銷只要當成把準備好的量都賣光來看就好，跟巧克力那種人擠人的買法有區隔就是了)

「能賣出就不錯了不是嗎」

「話說回來，單純來說惠方卷罕見又好吃好吃所以才賣不出去的嗎？

這和移節祭沒啥關係吧？」

對於艾爾尖銳的指謫，深深地刺進了我那純潔的心靈。

「客層也很偏向某一邊了呢……」

為了慎重起見拜託去偵查阿爾提裡歐先生他們情況回來的剛臣先生，對注意到的事情開始指謫了。

「太偏向某一邊了？」

「對，聖瓦倫丁祭那邊幾乎都是女性。而我們這邊，則以年長的男性和男性比較多」

「照這麼說的話……」

我們這邊女性的客人很少。

感覺上因為是罕見的食物而打算買回去給家人的老人們，而男性就都是肉體勞動系的人比較多。

「這個，單手就能吃了，份量也很足夠」

「鮑麥斯特伯爵大人，這和移節祭無關喔。雖然是暢銷，但……」

「會變成是在做工的父親們的食物吧」

確實，暢銷是暢銷但並不是我所謀求的方向。

惠方卷，對工作忙碌的男性來說用一隻手就能吃了而且也能填飽肚子因此才會大受歡迎。

「能通往幸福的道路，雖然吃一支就夠了，但對女性、小孩及老人家來說會很吃不消的吧？」

「這麼說來……」

為了便宜大碗才把惠方卷做這麼大支的，但或許是這樣的原因才得不到女性和小孩的支持。

要是被說如果能切來吃就好，那我也無話可說了。

「買回去給孫子當禮物嗎」

「有老人的支持」

「如果有好幾人吃的話，切一切就好了」

明明很暢銷，但不知為何卻無法釋然。

那是因為我只是在販賣新的食物，而且還無法跟打倒聖瓦倫丁祭產生連結的關係吧？

「但是，不是暢銷就好了吧？因為這也能促進我們這兒的特產品米和南方的眾多物產的銷售」

這個惠方卷所使用的米是來自增產中的鮑麥斯特伯爵領，煙燻鮭魚也是一樣養殖事業是使用了棲息在鮑麥斯特伯爵領地的南方鮭所製作出來的。

「對於瑞穗公爵領來說，因為能促進海苔和醋的銷售所以連領主大人都很高興」

「那麼，就結果而言是非常成功的吧」

「就如艾爾溫說的那樣。太好了，鮑麥斯特伯爵大人」

唉，雖然買賣上是成功了，但還是有股無法釋然的感覺。

稍微看了一下情況，在我自己親自去阿爾提裡歐商會所營運的點心屋偵查的時候，發現巧克力和使用它的點心非常熱銷。

而且，女性的比例比較高。

總的來說，幾乎都是女性。

「這種最小的巧克力請給我三十個」

「謝謝」

「是女性贈送巧克力給男性的活動啊。這麼做也很有趣呢」

貴族的婦女，大量地將最便宜的巧克力買下來了。

大概，是要分送給家臣和傭人的吧。

「老爺，就買這種大小的回去吧」

不妙，突然的開端，感覺越來越接近日本的情人節了。

這樣下去的話，再過幾年情人節就會變成多數男人的悲傷之日了。

「（但是，能夠阻止下來的方法……）」

如果巧克力暢銷的話，自然會滋潤著鮑麥斯特伯爵領，同時也會維繫著開發的促進。

我身為領主，不能去破壞這個活動。

「（為什麼這麼進退兩難……）」

之後我回去後販售著炒豆和惠方捲了，雖然努力地推薦模仿節分的移節祭，不過，就是無法消除掉沒能熟知過於老氣這個缺點。

雖然惠方卷很暢銷，但炒豆卻賣不太出去。

「能當下酒菜吧？」

只有少數的酒鬼，會因為便宜而買下來。

當成下酒菜，似乎會被認為是開心果或腰果的低價版。

「把這種炒豆拿來撒嗎？

鮑麥斯特伯爵大人，我無法認同您這麼浪費食物的喔」

不知為何被來買炒豆的老人家說教了。

因為是正論所以無法辯解。

「怎麼了？ 一臉不高興的表情啊，鮑麥斯特伯爵大人」

由於惠方卷賣完而在關店時，這時候阿爾提裡歐先生露面了。

「聖瓦倫丁祭在實行了。不得不辛勤地增產巧克力。鮑麥斯特伯爵大人也一樣，新的食品不也是賣得很好。但是，賣的是自己領地所自行生產的食材，如果能介紹新料理的話……。居然也有這種銷售法，真是讓我感到佩服」

說不定是巧克力很暢銷心情才這麼好，不過，阿爾提裡歐先生總覺得有了相當大的誤解。

我是為了賣鮑麥斯特伯爵領所生產的大米和煙燻鮭魚，才會舉辦惠方卷的介紹和銷售。

確實，一邊介紹烹調的方式一邊販賣食材在日本是很普遍的，但是，我為的是要打倒聖瓦倫丁祭，才去販賣惠方卷的。

就是知道是這麼回事，我才會感到焦躁。

4-2.

「話說，突然想到能開一家把鮑麥斯特伯爵大人常吃的飯糰、瑞穗的和式點心、豆皮壽司等東西搭配在一起的店舖。那麼ーーー，不知道能不能借的到手藝人呢來去找瑞穗公爵商量看看吧」

結果，不論是巧克力或是惠方卷都很暢銷，明明我沒損失反而還大賺一筆，但不知為何卻只有滿腹的敗北感。

再這樣下去的話，聖瓦倫丁祭就真的有很高的可能性會變成情人節了。

但是，我身為領主卻陷入無法阻止這件事發生的窘境。

我不得不承認我失敗了。

「收一收回去吧……」

「真是奇怪的傢伙，明明生意這麼成功」

「是啊，鮑麥斯特伯爵大人。即使在我們瑞穗公爵領，就算投資在生意上還有很多蒙受很大損害的人。還是有很多被揶揄是『武士經商』的。光能成功就很厲害了」 (註:サムライ商売又稱お侍商売，是對不太會做生意之人的揶揄，因為很拚命在磨練自己的本事和自身，其他方面給人有一種就不靈巧、不在意、經營第二的印象。知道是這種意思就好因為正確的解釋找不到)

「我回來了」

回到宅邸的我，還處在很失落的狀態。

聖瓦倫丁祭沒有搞砸掉，如果未來不受歡迎的男性去考究聖瓦倫丁祭的由來和傳播開來之人的話，我絕對會被當成戰犯來指責的。

我盡力了……卻是一整個討厭結果。

不能在替自己找藉口了。

「鮑麥斯特伯爵大人，遙過得好嗎？」

因為來這裡幫忙好幾天的關係，我招待剛臣先生來家裡了。

他好像因見到好久不見的妹妹而感到很高興。

「我過得很好喔」

和艾爾的婚禮之日也決定好了，目前則都在做婚禮的準備以及和埃莉絲她們一起在做菜比較多。

公開場合上，也有鮑麥斯特伯爵夫人們的侍女兼護衛的立場。

「哥哥大人嗎？ 好久不見了」

「遙，過得好嗎？ 艾爾溫有沒有花心？

有沒有被欺負？」

「完全沒有」

因為她是待在埃莉絲她們身旁的外國人如果有人敢欺負遙的話，那傢伙會在鮑麥斯特伯爵領待不下去的。

會是艾爾嗎？

不管怎麼看他都是被太太踩在腳底下，連錢和胃袋都被掌握住了，但他本人並沒有感到任何的不滿。

「是嗎，有精神就好」

「哥哥大人，是和領主大人一起在販賣太卷嗎？

在王國也很暢銷嗎？」

「有配合王國人們的口味進行改良了，因為少見所以都賣光了喔」

「這樣呀，那真是太好了呢。啊，對了」

遙好像想起什麼來一樣，將手裡拿著帶有緞帶的箱子交給剛臣先生，接著還遞給了艾爾。

「阿爾提裡歐先生，因為舉辦了聖瓦倫丁祭，所以我也試著去買了」

「「太好了ーーー！ 聖瓦倫丁祭真棒！」」

從遙那裡收到了巧克力，這二個人不知為何喜出望外到吶喊起來了。

「聖瓦倫丁祭是有必要的節日啊」

「恵方卷，正常賣可以了」

二個人，都在拿到一個巧克力後就立刻背叛了我。

不對，那種說法有誤。

因為搞砸聖瓦倫丁祭的這個野心，只有我知道而已。

「（這下子只剩我一個人了啊……但是！

我決定要把野望放在消滅情人節上！）」

沒錯，這是我要獻給未來男性們的戰鬥！ 

就算只有我一個人……當思緒都在想這件事的時候……。

「親愛的，歡迎回來」

埃莉絲她們都聚在一起迎接我。

「配合阿爾提裡歐先生所舉辦的聖瓦倫丁祭，我們今天也有準備巧克力喔」

「有熱巧克力、巧克力冰淇淋、巧克力蛋糕、巧克力脆片，還有其他各種各樣用它做出來的東西喔，威爾」

「飯後大家一起吃吧，威爾」

「在埃莉絲大人的帶領下，大家一起做的」

「這是我相當有自信之作喔」

什麼，埃莉絲他們居然有替我準備了巧克力。

「親愛的，您討厭巧克力嗎？」

「沒有，我很喜歡喔。從以前開始就有在吃了不是嗎」

「說的也是呢。晚飯後一塊吃吧」

「好」

我，立刻改變想法了。

在聖瓦倫丁祭中沒得到巧克力？

那是在撒嬌。

而且，在一到二年的間距中是因沒有得到巧克力而產生的大吵大鬧。

人，只要活著總有一天會遇上好事的！

「（我最喜歡聖瓦倫丁祭了！）」

在那之後，聖瓦倫丁祭在我死後，就幾乎和日本的情人節的形式一樣落定了，恵方卷則和其他的海苔卷及豆皮壽司一起在王國紮根了。

阿爾提裡歐先生所開設的店舖生意也相當興隆。

然而，這個些人之中，或許有個人物得到最多他就是……。

5.

「父親，在鮑麥斯特伯爵大人的宣傳下惠方卷很暢銷。請您也將飯糰和其他的卷物拿到王國去賣吧」

「這樣啊。我們藤林家，雖然從基層成為上士是件好事，但也因為沒錢而很困擾。對副業該做什麼感到很迷惘，不過，就以分家之主的名義去做海苔的批發和販售吧」

「不會甜的瑞穗茶，作為製作糕點材料的抹茶預料也會有需求。乾貨也能拿來用吧？」

「是啊」

即使在瑞穗公爵領內，就算是基層上士程度的官位各種開銷也是很兇的，所以生活也很辛苦。

藤林家，在剛臣提出建議下，開始批發並賣起海苔、茶葉和乾貨後使得大量的錢財都堆築起來了。
