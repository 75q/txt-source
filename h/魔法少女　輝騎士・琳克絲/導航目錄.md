# CONTENTS

魔法少女　輝騎士・琳克絲  
魔法少女セイバーナイト・リンクス　公開スライム浣腸排泄刑に堕つ  
魔法少女　輝騎士・琳克絲  ~史萊姆浣腸排泄之刑公開墮落~  

作者： マキザキ  



- :closed_book: [README.md](README.md) - 簡介與其他資料
- [格式與譯名整合樣式](https://github.com/bluelovers/node-novel/blob/master/lib/locales/%E9%AD%94%E6%B3%95%E5%B0%91%E5%A5%B3%E3%80%80%E8%BC%9D%E9%A8%8E%E5%A3%AB%E3%83%BB%E7%90%B3%E5%85%8B%E7%B5%B2.ts) - 如果連結錯誤 請點[這裡](https://github.com/bluelovers/node-novel/blob/master/lib/locales/)
-  :heart: [EPUB](https://gitlab.com/demonovel/epub-txt/blob/master/h/%E9%AD%94%E6%B3%95%E5%B0%91%E5%A5%B3%E3%80%80%E8%BC%9D%E9%A8%8E%E5%A3%AB%E3%83%BB%E7%90%B3%E5%85%8B%E7%B5%B2%20%20~%E5%8F%B2%E8%90%8A%E5%A7%86%E6%B5%A3%E8%85%B8%E6%8E%92%E6%B3%84%E4%B9%8B%E5%88%91%E5%85%AC%E9%96%8B%E5%A2%AE%E8%90%BD~.epub) :heart:  ／ [TXT](https://gitlab.com/demonovel/epub-txt/blob/master/h/out/%E9%AD%94%E6%B3%95%E5%B0%91%E5%A5%B3%E3%80%80%E8%BC%9D%E9%A8%8E%E5%A3%AB%E3%83%BB%E7%90%B3%E5%85%8B%E7%B5%B2%20~%E5%8F%B2%E8%90%8A%E5%A7%86%E6%B5%A3%E8%85%B8%E6%8E%92.out.txt) - 如果連結錯誤 請點[這裡](https://gitlab.com/demonovel/epub-txt/blob/master/h/)
- :mega: [https://discord.gg/MnXkpmX](https://discord.gg/MnXkpmX) - 報錯交流群，如果已經加入請點[這裡](https://discordapp.com/channels/467794087769014273/467794088285175809) 或 [Discord](https://discordapp.com/channels/@me)


![導航目錄](https://chart.apis.google.com/chart?cht=qr&chs=150x150&chl=https://gitlab.com/novel-group/txt-source/blob/master/h/魔法少女　輝騎士・琳克絲/導航目錄.md "導航目錄")




## [短篇](00000_%E7%9F%AD%E7%AF%87)

- [全](00000_%E7%9F%AD%E7%AF%87/%E5%85%A8.txt)

