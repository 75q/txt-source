經過了三天兩夜的地下迷宮攻略洗禮
我們一行人終於平安順利的回到了地面之上。

明天開始學科授課。

「話說回來，雖然這次是以嘗試看看的心情才在地下迷宮待了三天，下次我們還是應該先商量好，作足了準備再行動」
「何況珂露雪與我們年級不同，課程上的安排也需要再做調整。⋯⋯珂露雪？」
「誒？　啊、恩、恩、說的也是呢」
「『⋯⋯？』」

總覺得珂露雪表現地不太尋常。

都不怎麼加入我們的話題。
好不容易說到話，卻不知為何突然臉紅，倆人的對話完全對不上。

「那麼、我們之後該在哪裡集合討論呢？」
「該約在哪裡好呢？　雖然說宿舍裡也有空間可以交流啦⋯⋯」
「既然如此，我想到了一個好地方喔」

決定好集合時間，暫時先返回宿舍。

───

與艾莉婭、珂露雪分別後，回到了自己的私人空間

維納斯擬人化，變成了幼女的模樣。

「噢─耶！終於久違地能夠自由行動啦！」

待在地下迷宮的期間
因為有珂露雪在所以一直乖乖保持劍的姿態。

或許因此累積了許多壓力
維納斯一下子跑到床上，在上面活蹦亂跳的。

簡直就是個小孩子嘛。

「吾乃劍，雖說並沒有因為身為劍而感到痛苦，但是偶爾這樣能自由地活動筋骨也不錯呢。噢─耶！」
「聽起來是不錯，但是盡量不要發出太大的聲響。可能會被隔壁房的珂露雪聽到。
如果被別人知道我把幼女帶進房間就糟了。
雖然感覺現在經過這次攻略地下迷宮後，已經開始微妙地不停避開我了」
「是被避開了嘛⋯⋯庫庫庫」

不知道為什麼，維納斯一臉不懷好意的表情發出笑聲。

「是怎樣啦？」
「不不不、什麼事都沒有喔～」

在這之後，我們在校舍內設置的咖啡廳裡集合。
然而卻不由得在門口前止住了腳步。

「看起來這麼高級的地方，沒問題嗎？」

珂露雪不禁苦笑了出來。

「沒事的。這裡只要是在校學生都能自由出入」
「真的嗎」
「即便如此，你仔細瞧，會來這裡的學生大多都是平民出生喔？　貴族的學生通常會選擇收費更高、更奢華的場所」
「⋯⋯原來是這樣啊」

以我這種貧困人家出生的眼光來看、怎麼看都像是很高檔的店面啊⋯⋯⋯

話說一開始進來的時候，也被學生宿舍裝潢的豪華度嚇到了

讓我再次意識到，我還真是來到了一個不得了的校園內讀書。

「那麼，關於什麼時候開始作為學生小隊進行活動的議題，
日程上的安排配合上其實並非難事。

這其中的緣由，便是因為無論一年生還是二年生

學科的必修課程講座都統一固定排定在每周的前幾天進行」

有時實技課程為了更深切探討課題，也會實地走訪，走出城市之外好幾天。

因此，每周的後半部都會刻意空出來。

「原來如此。若是以武官作為未來志願的學生，就不用在每周的後半部接受專門的授課講座了」
「恩」

珂露雪又接續下去補充。

「平日課程的出席分數只佔總成績的一小部分，所以只要考試分數夠高就能賺取學分。透過學生小隊分組，也有學生會將每堂課下去劃分，其中的某個人去聽某堂課，再將他的筆記讓全學生隊伍裡的人一同學習。當然這些都是校內默許的成規」
「嘿─」

果然有前輩在真是受益良多。

非常在意的情報＋α通通馬上入手。

───

這時，店內忽然傳來一陣吵雜。

想看發生什麼事，而將視線轉向門口，是在入學典禮上時見過的身影。

「菲、菲奧拉王女殿下！？」

店員發出了驚慌地發出高音。
沒錯。

那人便是持有這個国家的王位繼承權
貌美如花的公主，菲奧拉・萊亞・聖德古拉王女殿下。

她毫不在意周圍的目光，堂堂正正地從門口走進來。

啊，說起來，這裡不算是很高級的店面啊⋯⋯⋯

與她相較之下，自己還真可悲，這種想法油然而生。

但是她為什麼會出現在這裡呢？
才正思考著，她便朝著我們桌位的方向前進。

『嚯嚯！這難道是在尋找汝嗎！是好機會啊！』

喂，怎麼可能呢。
雖說是在同一所學校裡讀書

但我不認為一国的公主會特意專程跑來找我這種平民出生的大叔。

所以說，就是來找艾莉婭的吧。
確實，艾莉婭是原貴族之女。
過去和公主相識也不足為奇。

但是，我的猜測卻完全落空。
公主綻開像花一般甜美的笑容，而那個對象居然是坐在我旁邊的少年

「找到你了喔、珂露雪大人」

⋯⋯大人（さま）？

「是、是的、菲奧拉殿下⋯⋯」

珂露雪板起臉孔，立刻從座位上站起來，雙腳一跪。
卻被公主制止，像是鬧彆扭似地，一臉不滿的嘟著櫻桃小嘴。

「真是的，本公主都已經說過好多次了。要叫本公主為菲奧拉才對」
「不、不是、直呼其名、這種事還是⋯⋯」
「請不要使用敬語。本公主與珂露雪大人之間的關係不需要這些東西」
「嗯、嗯嗯⋯⋯我知道了⋯⋯菲奧拉桑⋯⋯」

面對滿臉笑容的公主，珂露雪平常開朗的模樣仿彿是個謊言。

話說回來，公主居然會與平民之身的珂露雪親密接觸。
仔細一看，身後也沒有服侍的人陪同。

入學典禮時的說辭，還真的不是社交辭令啊。
還是說，珂露雪雖然出身平民，與我不同，實際上是某個大富豪的孩子嗎？

⋯⋯不對，好像有說過同為鄉下出身。

「話說回來，珂露雪大人。那件事考慮的如何呢？　本公主為了早點聽到答覆，而一直尋找你的身影呢」

聽起來像是個十分重要的約定呢。
我們這些局外人還是先離開不打擾他們，向珂露雪使了個眼色
他卻搖了搖頭。

不只如此，眼神還強烈地發出了「別丟下我一個人啊」的求救訊號。

公主用異常熱情的聲音說道

「珂露雪大人，請務必要加入本公主的學生小隊」