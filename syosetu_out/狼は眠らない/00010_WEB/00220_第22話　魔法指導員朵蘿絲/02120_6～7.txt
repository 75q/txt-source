６

「那麼，何謂魔法呢。所謂魔法，是將體內的魔力，以一定的程序向體外放出，透過控制那放出的方式，引起某種現象」

朵蘿絲停頓了一下，等待聽講者們將自己的話映入腦海中。

「就算不使用魔法也能生火。但只要能妥當地控制魔法，就能給予火焰只有魔法辦得到的突破力和爆發力。也就是說，魔法這神明授予人類之恩寵，其現象展現的姿態越是偏離日常，就越有魔法性」

朵蘿絲在大木板上，以木炭寫出單字。

攻擊魔法
防禦魔法
回復魔法
探查魔法
輔助魔法

「這是魔法的分類。啊啊，有看不懂字的人嗎？請老實回答。看不看得懂字，和魔法的才能沒有任何關係」

有三人舉起了手。

「很好。那麼，就以看不懂字的人也能理解的方式來進行講座吧。剛剛寫上的字，從上面開始念是，攻擊魔法，防禦魔法，回復魔法，探查魔法，輔助魔法。所有的魔法，都會屬於其中一種分類」

這魔法的分類，讓雷肯很困惑。希拉教的分類是，身體系、精神系、知覺系、創造系、空間系、光熱系、特殊系、神聖系。

「這個順序，也是魔法的優劣順序。攻擊魔法才是最為優秀的魔法，而輔助魔法是最劣等的魔法，是魔法性很低的魔法」

魔法的種類有分優劣這情報也是第一次聽到。

「不過，輔助魔法中，雖然有只是生火、點燈等其他的手段也能實現的，不怎麼魔法性的魔法，但也有冷卻物體，讓眼前的東西看不到等等，非常魔法性的魔法。分類所分的優劣，只是為了方便而已」

朵蘿絲在木板下側，寫上了新的文字。

迷宮
恩寵品

「在迷宮，能透過詠唱咒文來移動到其他階層。知道移動咒文的人請舉手」

約半數的冒険者舉起了手。

「在迷宮，就算是沒有魔力的人，也能透過詠唱咒文在階層間移動。此外，迷宮掉落的恩寵品中，存在只要詠唱咒文，就連沒有魔力的人也能得到魔法性效果之物品」

朵蘿絲在寫在木板上的〈迷宮〉和〈恩寵品〉的文字上劃了橫線。

「這些現象，並不符合我剛剛提到的魔法之定義。因此，這些雖然有魔法現象，也不會被稱為魔法，引起這些的咒文，也會被稱為非魔法咒文以區別」

以抹布擦掉了〈迷宮〉和〈恩寵品〉的文字。

「本日的講座，不會講解非魔法咒文。請先好好理解這一點。那麼接下來，就來仔細看看攻擊魔法吧」

這次，在寫了〈攻擊魔法〉的下側，補上了文字。

光系
炎系
雷系
水系
冰系
土系
闇系

「攻擊魔法被分類為，光系、炎系、雷系、水系、冰系、土系、闇系這七系統」


７

朵蘿絲接續了說明。是雷肯第一次聽到的魔法分類。具體的魔法名，有些有記載在希拉寫的一覽表上，有些沒有。在聆聽是何種魔法的說明時，有些也推測得出來是什麼。

根據朵蘿絲的話，這扎卡王国是魔法大国。然後，這国家之所以能建立並維持廣大的版圖，最大的原因，在於魔法和魔法使。
攻擊魔法才是最偉大的魔法，而在那之中，能將劍和槍打不倒的非實體系的妖獸打倒的光系，位於攻擊魔法的最上位。
造成睡眠、混亂、硬直等，不用魔法也能引起的，作為攻擊魔法的序列比較低。

這種序列的排法，雷肯大有異議。
〈睡眠〉的有效性，在尼納耶迷宮深深感受到了。〈混亂〉跟〈硬直〉，在戰鬥中也毫無疑問能發揮很可怕的威力。
相對的，朵蘿絲放在攻擊魔法最上位的光系的〈閃光〉，原來如此，對魂鬼族的妖魔可能很有效吧，但在對人戰和普通的魔獸戰上，能做的也就只有致盲，之前是這麼聽到的。

此外，這分類，感覺讓實際的魔法習得變得更複雜了。
如果是希拉的分類，就會以魔力的質來分類，除了特殊系和只要有魔力就誰都能學會的〈燈光〉和〈著火〉之外，習得了某種魔法，就能知道在同系統的所有魔法上都有適性。

然而，如果是朵蘿絲的分類，魔法的分類並沒有與適性對應，就很難知道有哪些魔法可能習得。
看來，是非常觀念性，形式性的分類，而且感覺給魔法分優劣也沒什麼意義。

雖說如此，〈水系攻擊魔法〉的〈水刃〉，以及〈冰系攻擊魔法〉的〈冰彈〉等等，應該是至今都不知道的魔法，〈有些魔獸有很強的魔法抵抗，恩寵裝備中也有能防禦魔法攻擊的，對這類敵人，水系攻擊魔法和冰系攻擊魔法也都有效〉這說明，也非常讓人感興趣。

那魔法務必要學學看，雷肯想著。