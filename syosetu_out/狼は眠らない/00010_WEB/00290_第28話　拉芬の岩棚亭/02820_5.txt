隔天，雷肯和阿利歐斯在姫龜一刻下來吃飯，在姫龜二刻離開了宿屋。

之後，〈遊手好閒〉出門了，然後在傍晚回來。

〈遊手好閒〉是三人隊伍。
短槍使秦格。
雙斧使布魯斯卡。
魔法使喬安娜。

〈遊手好閒〉的三人，有稍微特別的經歷。
全員都生於這茨波魯特，在茨波魯特成了冒険者，但茨波魯特迷宮對新手冒険者來說太難纏了。他們各自在外地的迷宮增強了實力。在這時間點，三人不認識彼此。

之後，三人在索特爾的迷宮相遇了。然後組成了隊伍。組隊後才發現是同鄉。
而他們三人，都有總有一天要探索茨波魯特迷宮這目的。
他們一起回到茨波魯特城，開始探索〈劍之迷宮〉。

在〈拉芬的岩棚亭〉，已經住三年了。
他們慢慢地向下層持續挑戰，現在是〈深層組〉。已經是城鎮裡無庸置疑的一流冒険者了。

「能幫忙把洗澡水燒開嗎」

秦格的措辭有些老成。再加上魁武的身材，發達的肌肉，禿上去的頭部，看起來大上了十歳。

「我要第一個進去喔」

喬安娜身為女性有個好體格。粗魯的說話方式也幫了忙，讓她看起來比實際年齡大上五歳左右。

「那，我要第二個」

雙斧使布魯斯卡身材標準，能說是沒有特徵的人物，但由於平常的舉止態度很穩重，看起來果然比實際年齡大了七，八歳。

〈遊手好閒〉看起來是聚集了三十五歳左右的冒険者的老練隊伍。但他們今年其實都才二十七歳，是還能再更進一步的團體。

要說為何納可知道他們的年齡，是因為納可的父親和布魯斯卡的父親是友人，而教導布魯斯卡冒険者的技術的就是納可的父親。然後，納可的父親，將在這迷宮的深層入手的雙斧讓給了布魯斯卡。因為這些原因，納可很了解布魯斯卡，而這布魯斯卡和另外兩人同年，所以納可知道全員的年齡。

浴室要從食堂深處的門進去。浴槽是筒型，會在下部添柴加熱。牆壁、天花板和門姑且都有，所以冬天也能用。位在庭院角落深處的場所，沒辦法從道路看到。在夏天會把大窗戶整個打開，能眺望庭院的蔬菜和花朵。〈拉芬的岩棚亭〉是能泡澡的宿屋。

「納可先生。我們到九十二階層了喔」
「喔！那還真是了不起。幹得好阿，孩子」
「謝謝。但是，別說孩子了」

納可和布魯斯卡的年齡差大到能做父子，從布魯斯卡還是嬰兒時就認識他了。也給過劍的指導。所以現在也會不小心像還是少年時那樣稱呼。
這天外面也很冷，〈遊手好閒〉的三人跳入了澡堂中取暖，舉起慶祝的酒盃。

雷肯和阿利歐斯沒有回來。

是去別的宿屋了吧。

感覺會在這裡住一陣子，所以讓納可感到有些意外。

「小阿利歐斯不在就好寂寞呢」

妮露一邊收拾一邊嘟嚷。

「說不定，在探索時過夜了呢」
「不，沒可能吧」

這迷宮不太會有人在探索時過夜。

不論是在探索哪一階層，進入向上階梯並移動到地上階層都很簡單。然後，不管是把魔石或寶箱當目標，還是要打倒大型個體來向更深的階層前進，戰鬥一天後離開迷宮，療傷，恢復體力和精神力，保養裝備，補充消耗品，慢慢地睡覺，然後在隔天再次探索，都會比較有效率。

但是，挑起長期戰的話就不同。

所謂長期戰，就是暫時進入房間和魔獸戰鬥，然後離開房間治療傷口，消除疲勞，有時會吃個飯，接著再進入房間和魔獸戰鬥，像這樣反覆進行的戰法。
雖然不知道其他迷宮的情況，但至少在這迷宮，魔獸不會從房間出來。而迷宮的魔獸就算受到傷害也不會回復，就算會，也只能回復得非常緩慢。所以這種戰法才能成立。

當然，只有在對上大型個體時才需要這種戰法。如果對上並非大型個體的普通魔獸，還得用這種戰法的話，那階層對那隊伍來說就還太早了。而且會需要收支不平衡的經費，所以沒有用這種戰法的意義在。只有在為了去更深的階層而對上大型個體時，這戰法才有意義。

雷肯和阿利歐斯，今天是第一次挑戰〈劍之迷宮〉。

在淺階層不會有長期戰。說到底，不這麼做就進不了下個階層的隊伍，挑戰〈劍之迷宮〉還太早了。此外，淺階層會接連出現挑戰者，所以獨佔大型個體的房間不會被容許。兩人僱用了鼠，所以應該知道這件事。

但是，不覺得那兩人會在淺階層死去。
所以納可覺得兩人移到別的宿屋了。

得知這是誤會，是在隔天傍晚。