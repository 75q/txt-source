王家在過去有個被稱為〈白雪花之姫〉的美麗公主。其名為洛蕾希亞。

不知為何，王不讓洛蕾希亞公主在人前出面。當然，王家的公主是不能輕率地在人前露面的，但快十四歳時也沒有舉辦披露之宴，就算高位貴族請願，也不會在茶席現身。
其實，洛蕾希亞公主是〈淨化〉持有者。就算被發現是〈淨化〉持有者，也不用擔心會受到不講理的待遇，但求婚之戰會變得很可怕吧，無法期望平穩的生活。

但越是隱蔽，貴族們對洛蕾希亞公主就越感興趣。有好幾個密探被派出，想素描洛蕾希亞公主的畫像，但恐怕沒有哪一家有成功。

想讓洛蕾希亞公主過上平靜人生的王，拜託了萊茵勒持家當主，秘密地讓洛蕾希亞公主成了萊茵勒持家的養女。萊茵勒持家的當主的正妃是洛蕾希亞公主的伯母，所以會幫忙保護洛蕾希亞公主吧，果然王家在關鍵時刻能依靠的是萊茵勒持家。

當時的王，恐怕也有暗示，能讓洛蕾希亞公主做萊茵勒持家繼承人的妃子就好了。
然而，洛蕾希亞公主對由於公務而在萊茵勒持家滯留的瓦茲洛弗家的次期當主一見鍾情，苦惱得連飯也吃不了。

感到非常為難的萊茵勒持家的當主，向瓦茲洛弗家的當主提議。瓦茲洛弗家的當主很困惑。應該說，次期當主已經有正妃了，不想在家中引起波瀾。
但還是被萊茵勒持家一再拜託，並以不能做正妃為由拒絶，在此前提下，瓦茲洛弗家秘密地將洛蕾希亞公主迎為妃子。

另一方面，有兩家為了讓出名的王家的美姫成為自家長子的妃子而行動了。是吉德侯爵因度爾家，以及司馬克侯爵霍托斯家。擁有良港，競爭王国屈指可數之富貴的兩家，宣告要獲得〈白雪花之姫〉，讓其他求婚者的呼聲沉靜了下來。

兩家曾因利益經常對立而激烈爭鬥過，但也有著與其他地域爭執時會相互支持共鬥的關係。兩當主在商量後，決定一起向王家提議下嫁，並交給王家判定，要選哪一家。

在兩家向王請願洛蕾希亞公主的下嫁的時間點，洛蕾希亞公主已經是萊茵勒持家的養女了，但當時的王国的經濟狀態很差，物資不足，確切地需要吉德和司馬克侯爵的協力。王家，應該說宰相府，便約定了會讓洛蕾希亞公主回到王家，嫁到兩家之一。並命令由兩家協議，決定要嫁到哪邊。

聽到這個的內務事務長官臉色發青了。因為，萊茵勒持家送來了報告說，將養女洛蕾希亞公主嫁到了瓦茲洛弗家，鑑於不想讓洛蕾希亞公主的身邊起騷動的王的意志，所以是秘密婚。
當時的宰相是，當時的萊茵勒持家當主的親弟弟，因此內務事務長官認為，他當然知道這件事才對，便沒有在定例的報告中口頭報告，只有轉到文件上。但是，宰相不記得有看過那文件。當然也就沒傳達給王。恐怕，是某個部下判斷，文件沒有必要讓宰相看，當作已處理了吧。

萊茵勒持侯爵被急遽找去，和王、宰相及內務事務長官共四人進行了協議。就算討論了一晚也提不出解決方案。

「就當作是亡故了吧」

如此道出的是宰相。就這樣，舉行了秘密的，而且是格式很高的葬儀，並傳達給吉德、司馬克兩侯爵家與高位貴族們，公主由於體弱多病而死去了。

瓦茲洛弗家很生氣。這也是當然的。
作為秘密婚雖然對瓦茲洛弗家很方便，但那終究只代表不會公開，該知道的人會知道，必須在該紀錄的地方紀錄為正式的結婚。從王家系譜和諸家系譜中都省去的話，瓦茲洛弗家就會沒有洛蕾希亞這位妃子。那就不是結婚。是野合。

但是系譜官室的高官中也有吉德、司卡克兩侯爵的親屬，只要經過該有的程序，終究能閱覽諸家系譜。宰相府不能冒那種危險。宰相和萊茵勒持家當主，對激動的瓦茲洛弗家當主低下了頭。

由於這些緣由，瓦茲洛弗家，只能把洛蕾希亞公主當作，並非從任何一個貴族家嫁來的妻子。
不只如此，還絶對不能讓他家發現洛蕾希亞公主的存在。因此，對家人們下了嚴命，要把洛蕾希亞公主及其孩子們，當作是不存在。