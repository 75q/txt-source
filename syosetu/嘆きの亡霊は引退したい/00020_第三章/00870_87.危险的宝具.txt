勸解一臉不情願地瞪著我的同伴，亞克等人快步從休息室離去。

與我期待的一樣，亞克聽取了我的請求。
因為內容的關係，我沒有說任何詳細的事，但他應該是從我的言辭和現狀中察覺到了我想說的事吧。

不愧是亞克・羅丹。帝國最強的獵人，度量也是最強。
我最喜歡如此萬能的亞克。他甚至能使用回復魔法？你能相信？

與一無所能的我大不相同。

高等級獵人和其隊伍成員通常都很強大，但若升上更高的等級，獨行獵人反而不斷增多。
例如，我們《嘆息的亡靈》成為獵人的契機，僅有三人的等級１０獵人，艾克希德・齊格恩斯也是獨行獵人。
突出的獵人往往都是他人無法跟上的存在。但也因此具有很高的萬能性。

亞克總是選擇適合隊伍成員的寶物殿。如果單獨行動，或是加入更強大的隊伍，大概能更快地提高等級吧。
雖說是后宮隊伍，但他仍然組著隊，這對超一流的獵人來說，或許是稀有的事。

但願亞克永遠不會忘記那份溫柔。從今以後也要麻煩你了。
當我正滿足於自己的交涉能力時，表情一直露出不悅之色的莉茲撅起嘴，搖了搖我的肩膀。

「好～狡～猾～。克萊伊醬，你太依賴亞克醬了！再多依靠我一點？喂！喂！」

「⋯⋯⋯⋯」

「盧克醬也不在，也不可能和西特進行模擬戰，緹諾又是雜魚，本領要退步了。好嗎？我什麼都願意做！」

莉茲像是在賣弄風情一樣蹭著我的身體。你是寵物之類的嗎？

你剛才說什麼都願意做是吧？⋯⋯請安份點。
從實力上來說，我對莉茲沒有意見，無奈她的脾氣太過暴躁。我深深地嘆了一口氣。

「莉茲你啊，知道我委託了什麼嗎？」

「當然知道。到那個臭小鬼那裡，把寶具偷來就行了吧？請交給我！」

莉茲謎之得意地露出笑容。倫理觀太淡薄了。
雖然我想要寶具，但不會為此犯罪。

像是要去旅行一樣激動的莉茲屈指數起來。

「比寶物殿要輕鬆喲。警戒的騎士也不過是一般人，雖然露西亞醬不在，結界方面會有些麻煩，不過在被抓到之前偷回來就行了吧？啊，對啦！帶著小緹去吧！」

請別這樣。
明明以前不是這樣的孩子，過於習慣暴力了。可能是有適應性吧。

面具已經沒問題了。亞克應該能帶回來。
身為良心的西特莉無語似地責備起姐姐。

「姐姐！克萊伊桑很為難哦！判斷面具的事交給亞克桑更合適應該是有理由的，而且我們有我們要做的事」

「要做的事？」

「籌錢」

對於立即回答的西特莉，莉茲瞪圓雙眼，放下了交叉著的腿。
可能是明白了什麼，她微微點頭，看向我這邊。

「⋯⋯⋯⋯啊～，是這麼一回事呢。那就沒辦法啦。也不能拜託亞克醬呢」

「如果不在露西亞醬回來之前補上存款的缺口⋯⋯正好不是嗎？」

什麼正好呢。拋開完全摸不著頭腦的我，話題不斷進行下去。
不愧是姐妹，有著從外部無法理解的共鳴也說不定。

莉茲站了起來，和剛才相反，心情看起來很好地對我笑著。

「⋯⋯原來如此⋯⋯克萊伊醬的計劃還是那麼完美，也許。嗯，明白。最好快點是吧？也許久違地有干頭？西特，提前做好準備哦」

「我知道的⋯⋯」

「那麼，我先去稍微活動一下身體啦。克萊伊醬，之後再見。我會為了拿出好的報告而努力的，所以等著我！」

莉茲一邊輕快地揮著手，一邊邁著輕盈的步伐離開休息室。看那個樣子，她應該不會跟隨亞克等人去入室搶劫了。
雖然不知道她打算努力做什麼，但西特莉在的話就不會引發大問題吧。

「那麼，克萊伊桑。我也走了。姐姐的控制請交給我」

「嗯嗯，是呢。穩妥地」

可以的話我也想幫忙，但是就算我在也只會礙事吧。
與內容空洞的鼓勵相反，西特莉輕輕地握住拳頭對我微笑。

§ § §

回到據點，亞克等人便準備起裝備。
話雖如此，亞克等人的本職可是獵人。即使沒有探索寶物殿的預定，也維持著最低限度的準備。

亞克既是劍士也是魔導師。不僅徒手戰鬥十分一流，而且能使用魔法。一般人自不必說，就算以大部分的獵人為對手也不會輕易輸掉。

容量比看上去要大的荷包型寶具『時空包』，除了擁有巨大的容量，還擁有防止收納之物腐爛的特殊效果，是羅丹家代代相傳的超高級品。
各種藥水自不必說，從食品到野營道具也是應有盡有，能應付所有的狀況。

「真的⋯⋯要去嗎？」

「不安嗎？」

看著擔心似地抬頭看向自己的同伴──神官優的灰色虹膜，亞克向她微微一笑。
儘管另外幾人當著隊長亞克並沒有抱怨，不過卻一臉不滿似地做著準備。

她們嫻熟的手法稱得上一流獵人，但最重要的是在準備上卯足了勁，就像將要前往高難度的寶物殿一樣。

如果只舉出一個《起始的足跡》的特徵，亞克會舉出的不是大部分成員都有完善的福利待遇，不是無法想像屬於新起氏族的成員的戰力，也不是高大的氏族建築，而是氏族Master偶爾帶來的『千之試練』吧。

試練會平等地降到每個人身上，亞克的隊伍當然也不例外。
不對，亞克的隊伍是氏族的Number2。非要說的話，受托做事的機會感覺上要比一般的成員更多。

無論是攻略多麼危險的寶物殿，也比毫無預備就突然下達的委託要好，這是亞克以外的成員的言論。無時無刻都不怠慢裝備的維護並做好最低限度的準備也有那個原因。

優・希拉吉端正的容貌不安地扭曲起來。

「是的。克萊伊桑⋯⋯那個，經常把亞克桑牽扯進去」

「他可是等級８，明明自己動手就行。亞克桑也太嬌慣他了」

和探索寶物殿時一樣身穿純白色長袍的伊莎貝拉深深地嘆著氣。
的確，亞克很少拒絕克萊伊的委託。
要是害怕意外就無法擔任尋寶獵人，而且下達的那些委託如果沒有人去做，確實會造成巨大的損失。

「既然莉茲和西特莉都在，不覺得應該自己動手嗎？」

聽到徵求同意的聲音，亞克露出了無奈的笑容。

「伊莎貝拉是說讓那兩個人接近艾克蕾爾小姐嗎？我可做不到那麼可怕的事啊」

「⋯⋯這個⋯⋯確實，比如莉茲，就算對方只有十歲也會認真吵架呢。而且絲毫不會考慮對方的地位」

「⋯⋯雖然聽起來很可怕，但是很有可能啊」

對於伊莎貝拉的話，表情嚴肅的劍士亞爾美露表示同意。
誰都知曉《嘆息的亡靈》的強大，同時也知道他們傍若無人的態度。不過身為同氏族的成員並且交情很久的亞克等人，卻知道廣為人知的評價有些保守。

他們就像血氣方剛的黑手黨一樣。

「露西亞和安塞姆在的話就是兩回事，但他們好像還沒回來──不，不對，即便如此，自己去的話不就行了？就算面對貴族，克萊伊也能輕易說服吧？」

伊莎貝拉使勁地搖頭抗議，但她的聲音卻不像剛才那般有力。
應該是理性上明白判斷很合理，但感情上很難接受吧。

儘管艾克蕾爾・格拉迪斯還是孩子，可她是貴族的孩子。擁有很高的自尊心。
更何況，對於被稱為亞克的競爭對手的克萊伊，艾克蕾爾有著強烈的不滿。雖然克萊伊應該能有辦法，但克萊伊和交情原本就深厚的亞克，哪邊能順利交流可是顯而易見。

「接受吧，伊莎貝拉。原本我們就和格拉迪斯家有關係。不如說，如果出現麻煩事就必須前去⋯⋯向克萊伊抱怨有些不合理呢」

有相當長交情的亞克，也無法讀懂克萊伊的思考。
但是，亞克以外的成員姑且不論，而在亞克看來，迄今為止的『千之試練』還不至於令人抱怨。

有足夠的力量去救援，動機也有。
整理好著裝，最後從時空包裡拿出一把劍。
白色啞光的劍鞘以及，暗金色的劍柄。幾乎沒有任何裝飾的長劍，即使在收入劍鞘的狀態下，也有著令人著迷的莊嚴氣氛。

初代羅丹使用的直劍型寶具。
和羅丹共同開創歷史、祓除災禍的聖劍──希斯特莉亞。
在眾多的劍型寶具中也是以最強聞名，是一把尚無斬不斷之物的無雙之劍。

本來是不能把武器帶進貴族的宅邸的，不過亞克擁有許可。
說到底，就算被沒收一兩把劍，亞克・羅丹的戰鬥能力也遠超那些護衛騎士，而格拉迪斯卿也明白這一點。

雖然被告知最好不要帶著劍去，但聖劍是從不離身的武器。
亞克相信著克萊伊，可同時也知道他莫名地隱瞞情報，並試圖布置試練的事。
不拔出來的話就沒問題吧。

準備完畢。之後只需要確認艾克蕾爾的情況。
此時，同樣準備結束的伊莎貝拉皺起秀眉說道。

「但是亞克桑。如果來不及的話『我會傷心』，不覺得太胡鬧了嗎？」

「⋯⋯⋯⋯伊莎貝拉真是正經呢。好啦，艾克蕾爾小姐現在應該在格拉迪斯卿的宅邸。趕緊出發吧」

「！？誒？什麼？剛才的，是我不好！？」

帶上驚慌失措的伊莎貝拉和向她投去白眼的同伴們，亞克朝著似乎在發生什麼的宅邸步行而去。

§ § §

花費兩億吉爾的巨額贏下的面具，驚人地令人毛骨悚然。

雖然聽說過傳聞，但如果看過實物，或許就不會參加拍賣了。
仿彿生肉絞合而成的面具會像活著一樣搏動，使得替艾克蕾爾領取的格拉迪斯家總管皺起眉頭。

因為艾克蕾爾小姐在競拍上贏了那個等級８的事，家中的女僕和管家都對艾克蕾稱讚不已，可在看到實物的瞬間，她的表情明顯變了。

效果未知的被詛咒寶具。還是得到鑑定師的危險判定的人肉面具。

帝都的宅邸。競拍結束後，艾克蕾爾一直悶在臥室裡。
拉上窗簾並關著燈的漆黑臥室裡只有艾克蕾爾一人。而且幾乎沒有家具，僅擺放著一張帶華蓋的大床和一張桌子。

開始的第一天，因莫大的恥辱和憤怒而忍聲哭泣起來。第二天則是發火用東西出氣，而現在只剩下深深的後悔。

家裡的用人多次來叫過自己，但都被怒吼驅散了。作為自尊心強的格拉迪斯子女，無法忍受自己現在的樣子被看到。

被施予同情。光是這樣就足夠難受了，得到的寶具卻是看著都討厭的東西，很讓人懷疑參展者的精神是否正常。
就算正面戰勝並連同名譽一起獲得，把這個給敬仰的亞克也太令人生厭。

如果剝下人臉上的皮膚並削去表層，就會變成這個面具的模樣吧。說是人肉面具還真恰當。

幾天前不惜代價也想要得到的寶具，現在則被棄於床旁邊的桌子上。

本來讓來的勝利是一種恥辱，但在看過實物的現在，艾克蕾爾的心中卻空空如也。

頭劇烈地作痛。雖然食物被放在房間前面，但自己基本沒有動過。
最近幾天艾克蕾爾總算恢復了冷靜。但是，躺在床上的身體卻癱軟無力，什麼也不想做。

精神變得憔悴，連對千變萬化懷有的強烈憤怒也不復存在。

我從今以後──怎麼辦才好？

意識朦朧地思考著。
憑借感情用事，卻欠下兩億的債務。雖然是家裡的錢，但約定過要還。

從今以後⋯⋯怎麼辦？

出售在競拍中贏得的人肉面具？不，沒有哪家商會會買吧。它的價格變得如此之高的主要原因就是艾克蕾爾本人。以超過購買價的金額不可能賣得出去。

按照預定送給亞克？怎麼可能。既不是贏來之物也不知道效果，即使把這樣的寶具送給亞克，也只會讓他為難。

扔掉？大費周章才得到的？

賣給《千變萬化》？那簡直像小丑一樣。
在橫插一腳並且勝利被讓出後，自己還有什麼臉面賣給他啊。
單是想像就變得想死起來，接著艾克蕾爾輕微乾嘔了一下。

沒有答案的問題在腦內盤旋著。
在床上改變睡姿，看向放在桌上的人肉面具。

對於難以忍受髒東西的艾克蕾爾來說，光是看著就會感到噁心。
起初，在聽到效果無法鑑定時，還嘲笑鑑定師不中用，可是在看到實物時，也深刻理解了鑑定師放棄鑑定的理由。

就算只是觸碰也很反感。精神正常的人才不會戴它吧。

想到這裡，艾克蕾爾的腦海裏閃過一個疑問。

為什麼《千變萬化》想要得到這個面具呢？

最初想要這個的是《千變萬化》。打算事前和參展者交涉來獲得，得知此事的其他獵人們和商會，還有艾克蕾爾參與了進來。

應該有相應的理由。
雖然聽過最強的寶具這個傳聞，可就算看到實物也不會想相信。

──想要，力量嗎？

「⋯⋯⋯⋯誒？」

黑暗之中。由於腦內忽然響起的聲音，艾克蕾爾彈坐起來。
不知從何處飄來一股寒冷的空氣。

低沉沙啞的聲音。沒有聽過的印象。
迅速地伸出手去拿像護身符一樣放在枕邊的劍。
平時能輕易碰到的劍好重。只是拉過來就感覺身體要被拽過去一樣。

──在看著。一直在看著哦。那份哀傷、悲痛、憤怒，以及──絕望。充滿罕見才能的靈魂。雖然肉體很脆弱──不過就妥協一下吧。承載吾的力量正合適。

「⋯⋯面、面具在⋯⋯說話？」

怎麼可能。儘管那個看著很討厭，但它只是一件寶具。寶具不可能會說話。
雖然拚命地說服自己，但視線就像固定在了桌上的人肉面具上一樣無法移開。

慌忙拔出劍架起。使用左手並挪動屁股後退。
與魔物和幻影有過多次戰鬥。可是，由於未知的恐懼，劍身不停地抖動著。

「不只是說話。弱小之人喲。吾乃推進人類者。給予脆弱之身希望者。單獨一人正合適──來履行『發生』的責任吧。『吾主』」

「！？」

隨後，在黑暗中，面具漂浮至高空。

不對，準確地說並不是漂浮。而是從左右伸出的無數觸手像手腳一樣撐著本體。

怎麼可能。所謂的寶具，必須由持有者來啟動。艾克蕾爾甚至沒有碰過它。

──那是危險的寶具喲。

以前，在寶具的競拍開始前夕來交涉的青年，他看起來疲憊的表情和聲音在腦海中閃過。
接著，人肉面具大笑起來，並飛向艾克蕾爾。